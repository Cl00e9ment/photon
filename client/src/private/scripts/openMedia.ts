import {formatDate, getMediaType, Media, setClassName} from './utils'
import {hideOverlay, showOverlay} from './overlay'
import {getMedias} from './loadMedia'

const mediaMountPoint = document.getElementById('media-insert')
const prevBtn = document.getElementById('prev')
const nextBtn = document.getElementById('next')
const closeBtn = document.getElementById('media-close')
const mediaName = document.getElementById('media-name')
const createdAt = document.getElementById('media-created-at')
const createdAtText = createdAt.getElementsByTagName('span')[0]
const uploadedAt = document.getElementById('media-uploaded-at')
const uploadedAtText = uploadedAt.getElementsByTagName('span')[0]
const downloadBtn = document.getElementById('media-download') as HTMLAnchorElement
const shareBtn = document.getElementById('media-share')
const shareNotif = document.getElementById('share-notification')

let current: Media | null = null
let prev: Media | null = null
let next: Media | null = null

// close overlay on back btn click
window.addEventListener('popstate', () => {
	if (current) {
		hideOverlay()
		current = null
		prev = null
		next = null
	}
})

// treat close btn click, the same as back btn click
closeBtn.addEventListener('click', () => {
	window.history.back()
})

shareBtn.addEventListener('click', async e => {
	if (!current) {
		return
	}

	e.stopPropagation()

	if (navigator.share) {
		try {
			await navigator.share({
				title: current.name,
				url: window.location.href,
			})
			return
		} catch (e) {
			console.error(e)
		}
	}

	if (navigator.clipboard) {
		try {
			await navigator.clipboard.writeText(window.location.href)
			shareNotif.textContent = 'Lien copié dans le presse-papier.'
			shareNotif.style.display = 'block'
			return
		} catch (e) {
			console.error(e)
		}
	}

	shareNotif.textContent = 'Le partage ne fonctionne pas sur cet appareil. Copie l\'URL manuellement.'
	shareNotif.style.display = 'block'
})

document.body.addEventListener('click', () => shareNotif.style.display = 'none')

prevBtn.addEventListener('click', () => {
	if (prev) {
		window.history.replaceState(null, '', '#' + prev.id)
		openMediaOverlay(prev)
	}
})

nextBtn.addEventListener('click', () => {
	if (next) {
		window.history.replaceState(null, '', '#' + next.id)
		openMediaOverlay(next)
	}
})

export function openMedia(media: Media) {
	window.history.pushState(null, '', '#' + media.id)
	openMediaOverlay(media)
}

export function openMediaFromUrl() {
	if (!window.location.hash) {
		return
	}

	const mediaId = window.location.hash.substring(1)
	window.history.replaceState(null, '', '/')

	const medias = getMedias()
	const media = medias.find(m => m.id === mediaId)

	if (!media) {
		return
	}

	openMedia(media)
}

function openMediaOverlay(media: Media) {
	const medias = getMedias()
	const mediaIndex = medias.indexOf(media)
	prev = mediaIndex > 0 ? medias[mediaIndex - 1] : null
	next = mediaIndex < medias.length - 1 ? medias[mediaIndex + 1] : null
	setClassName(prevBtn, 'disabled', !prev)
	setClassName(nextBtn, 'disabled', !next)

	const mediaType = getMediaType(media)

	let displayedMediaType: string
	switch (mediaType) {
	case 'image':
		displayedMediaType = 'photo'
		break
	case 'gif':
		displayedMediaType = 'GIF'
		break
	case 'video':
		displayedMediaType = 'vidéo'
		break
	}

	if (mediaType === 'video') {
		mediaMountPoint.innerHTML = `<video controls poster="/media/${media.id}?poster"><source src="/media/${media.id}" type="${media.mimeType}"></video>`
	} else {
		mediaMountPoint.innerHTML = `<img src="/media/${media.id}" alt="${media.name}">`
	}

	mediaName.textContent = media.name
	uploadedAtText.textContent = `${displayedMediaType} uploadée le ${formatDate(media.uploadDate)}`

	if (media.creationDate) {
		createdAtText.textContent = `${displayedMediaType} prise le ${formatDate(media.creationDate)}`
		createdAt.style.display = 'block'
	} else {
		createdAtText.textContent = ''
		createdAt.style.display = 'none'
	}

	downloadBtn.download = media.name
	downloadBtn.href = `/media/${media.id}`

	showOverlay('media', false, () => mediaMountPoint.innerHTML = '')
	current = media
}
