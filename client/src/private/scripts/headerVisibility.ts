const header = document.getElementsByTagName('header')[0]
const headerHeight = header.getBoundingClientRect().height
let lastScrollY = window.scrollY

window.addEventListener('scroll', () => {
	const scrollY = window.scrollY

	// "scrollY > headerHeight" is necessary so we never see what is "behind" the navbar
	if (scrollY > lastScrollY && scrollY > headerHeight){
		// scroll down
		header.classList.add('hidden')
	} else {
		// scroll up
		header.classList.remove('hidden')
	}

	lastScrollY = scrollY;
})
