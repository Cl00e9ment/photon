import {getMediaType, insertAtTheBeginning, Media, nodeFromHtml} from './utils'
import {openMedia} from './openMedia'
import videoIcon from 'bundle-text:../icons/video.svg'
import gifIcon from 'bundle-text:../icons/gif.svg'

const mainElement = document.getElementsByTagName('main')[0]

let medias: Media[] = []

function sortMedias() {
	medias.sort((a, b) => (b.creationDate ?? b.uploadDate) - (a.creationDate ?? a.uploadDate))
}

export async function loadAllMedias() {
	const res = await fetch('/index.json')
	const index = await res.json()
	medias = Object.values(index) as Media[]
	sortMedias()
	medias
		.map(m => createMediaElement(m))
		.forEach(e => mainElement.appendChild(e))
}

export function getMedias(): Media[] {
	return medias
}

export function insertNewMedia (media: Media) {
	medias.push(media)
	sortMedias() // TODO insert in sorted array
	insertAtTheBeginning(createMediaElement(media), mainElement)
}

function createMediaElement(media: Media): HTMLElement {
	const mediaType = getMediaType(media)

	const thumb = document.createElement('img')
	thumb.src = `/media/${media.id}?thumb`
	thumb.alt = media.name
	thumb.className = 'thumb'

	const name = document.createElement('a')
	name.className = 'media-name'
	name.textContent = media.name
	name.title = media.name

	const mediaOverlay = document.createElement('div')
	mediaOverlay.className = `media-overlay media-type-${mediaType}`
	mediaOverlay.appendChild(name)

	const wrapper = document.createElement('div')
	wrapper.className = 'media'
	wrapper.appendChild(thumb)
	wrapper.appendChild(mediaOverlay)
	wrapper.addEventListener('click', () => openMedia(media))

	if (mediaType != 'image') {
		const icon = nodeFromHtml(mediaType === 'gif' ? gifIcon : videoIcon) as SVGElement
		icon.classList.add('media-icon')
		icon.addEventListener('dragstart', e => e.preventDefault())
		mediaOverlay.appendChild(icon)
	}

	return wrapper
}
