import {DateTime} from 'luxon/src/luxon' // using "src/luxon" for better bundle size (maybe because tree shaking is more effective on source files)

export interface Media {
	id: string,
	name: string,
	mimeType: string,
	creationDate?: number
	uploadDate: number,
}

export function insertAtTheBeginning(node: Node, container: Node) {
	container.insertBefore(node, container.childNodes[0])
}

export function getMediaType(media: Media): 'video' | 'gif' | 'image' {
	if (media.mimeType.startsWith('video/')) {
		return 'video'
	} else if (media.mimeType === 'image/gif') {
		return 'gif'
	} else {
		return 'image'
	}
}

export function setClassName(element: HTMLElement, className: string, state: boolean) {
	if (state) {
		element.classList.add(className)
	} else {
		element.classList.remove(className)
	}
}

export function nodeFromHtml(html: string): Node {
	const wrapper = document.createElement('div')
	wrapper.innerHTML = html
	return wrapper.firstChild
}

export function formatDate(timestamp: number): string {
	return DateTime.fromMillis(timestamp)
		.setZone('local')
		.setLocale('fr')
		.toFormat("d LLLL y 'à' HH'h'mm")
}
