import {insertNewMedia} from './loadMedia'
import {hideOverlay, showOverlay} from './overlay'

// do not open image in browser
document.body.addEventListener('dragover', e => e.preventDefault())

// show upload icon
document.body.addEventListener('dragenter', e => {
	e.preventDefault()
	showOverlay('upload', true)
})

// hide upload icon
document.body.addEventListener('dragexit', e => {
	e.preventDefault()
	hideOverlay()
})

// upload via drag-n-drop
document.body.addEventListener('drop', async e => {
	e.preventDefault()
	uploadFiles(e.dataTransfer.files).catch(console.error)
})

// upload via button
document.getElementById('upload').addEventListener('click', () => {
	const input = document.createElement('input')
	input.type = 'file'
	input.accept = 'image/*,video/*'
	input.multiple = true
	input.click()
	input.addEventListener('input', () => {
		uploadFiles(input.files).catch(console.error)
	})
})

async function uploadFiles(files: FileList) {
	showOverlay('loading')
	for (let i = 0; i < files.length; ++i) {
		if (!await uploadFile(files.item(i))) {
			showOverlay('error', true)
			return
		}
	}
	hideOverlay()
}

async function uploadFile(file: File): Promise<boolean> {
	const formData = new FormData()
	formData.append('file', file)
	const res = await fetch('/upload', {
		method: 'POST',
		body: formData,
	})
	if (res.status === 200) {
		insertNewMedia(await res.json())
		return true
	} else {
		return false
	}
}
