document.getElementById('logout').addEventListener('click', async () => {
	const {status} = await fetch('/logout', {
		method: 'POST',
	})
	if (status === 200) {
		window.location.reload()
	}
})
