export const overlay = document.getElementById('overlay')

let currentOverlay: {
	name: string,
	element: HTMLElement,
	dismissible: boolean,
	onHide?: () => void,
} | null = null

window.addEventListener('scroll', dismiss)
overlay.addEventListener('click', dismiss)

function dismiss() {
	if (currentOverlay?.dismissible) {
		hideOverlay()
	}
}

export function showOverlay(overlayName: string, dismissible: boolean = false, onHide?: () => void) {
	if (currentOverlay) {
		if (currentOverlay.name === overlayName) {
			return
		}
		hideOverlay()
	}
	currentOverlay = {
		name: overlayName,
		element: document.getElementById(`${overlayName}-overlay`),
		dismissible: dismissible,
		onHide: onHide,
	}
	if (!dismissible) {
		document.body.classList.add('no-scroll')
	}
	overlay.classList.remove('hidden')
	currentOverlay.element.classList.remove('hidden')
}

export function hideOverlay() {
	if (!currentOverlay) {
		return
	}
	if (currentOverlay.onHide) {
		currentOverlay.onHide()
	}
	currentOverlay.element.classList.add('hidden')
	overlay.classList.add('hidden')
	document.body.classList.remove('no-scroll')
	currentOverlay = null
}
