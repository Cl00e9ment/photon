import './headerVisibility'
import './logout'
import './upload'
import {loadAllMedias} from './loadMedia'
import {openMediaFromUrl} from './openMedia'

(async () => {
	// prevent image dragging (to avoid triggering upload overlay)
	document.body.addEventListener('dragstart', e => e.preventDefault())
	await loadAllMedias()
	openMediaFromUrl()
})().catch(console.error)
