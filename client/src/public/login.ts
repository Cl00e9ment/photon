const form    = document.getElementById("login")
const key     = document.getElementById("key") as HTMLInputElement
const message = document.getElementById("message")

form.addEventListener('submit', async e => {
	e.preventDefault()
	const { status } = await fetch('/login', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			key: key.value,
		}),
	})
	switch (status) {
	case 200:
		window.location.reload()
		break
	case 403:
		showMessage('clé invalide')
		break
	default:
		showMessage('une erreur s\'est produite')
		break
	}
})

key.addEventListener('input', () => hideMessage())

function showMessage(msg: string) {
	message.className = 'show'
	message.innerText = msg
}

function hideMessage() {
	message.className = ''
	message.innerText = ''
}
