import {readFileSync} from 'fs'
import type {Media} from '../../client/src/private/scripts/utils'
import {DateTime} from 'luxon'
import superagent from 'superagent'
import path from 'path'

const legacyDataDir = '/* ADD PATH HERE */'

;(async () => {
	const medias = Object.values(JSON.parse(readFileSync(path.resolve(legacyDataDir, 'index.json'), 'utf-8'))) as Media[]

	for (const media of medias) {

		/* PROCESS MEDIA HERE */

		await superagent
			.post('http://localhost:8080/upload')
			.set('Content-Type', 'multipart/form-data')
			.withCredentials()
			.auth('updater', '/* ADD PASSWORD HERE */')
			.field('uploadDate', DateTime.fromMillis(media.uploadDate).toISO())
			.attach('file', path.resolve(legacyDataDir, 'originals', media.id), {filename: media.name})
	}
})()
