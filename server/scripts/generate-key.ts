import {randomBytes} from 'crypto'
import {genSaltSync, hashSync} from 'bcryptjs'

const key = randomBytes(30).toString('base64')
const keyHash = hashSync(key, genSaltSync())

console.log(`key:  ${key}`)
console.log(`hash: ${keyHash}`)
