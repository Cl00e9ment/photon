import {readFileSync, unlinkSync, writeFileSync} from 'fs'
import path from 'path'
import {Media} from '../../client/src/private/scripts/utils'

// PARAMS

let errored = false

export const mediaId = getEnv('MEDIA_ID')
export const dataDir = getEnv('DATA_DIR')

function getEnv(key: string): string {
	if (!process.env[key]) {
		console.error(`${key} env variable isn\'t set`)
		errored = true
	}
	return process.env[key]
}

if (errored) {
	process.exit(1)
}

// DELETE

const indexPath = path.resolve(dataDir, 'index.json')
const medias = JSON.parse(readFileSync(indexPath, 'utf-8')) as {[key: string]: Media}

unlinkSync(path.resolve(dataDir, 'originals', mediaId))
unlinkSync(path.resolve(dataDir, 'thumbnails', mediaId))
if (medias[mediaId].mimeType.startsWith('video/')) {
	unlinkSync(path.resolve(dataDir, 'posters', mediaId))
}

medias[mediaId] = undefined
writeFileSync(indexPath, JSON.stringify(medias), 'utf-8')
