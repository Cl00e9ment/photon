let errored = false

export const devMode = process.env.NODE_ENV === 'development'
export const keyHash = getEnv('KEY_HASH')
export const dataDir = getEnv('DATA_DIR')

function getEnv(key: string): string {
	if (!process.env[key]) {
		console.error(`${key} env variable isn\'t set`)
		errored = true
	}
	return process.env[key]
}

if (errored) {
	process.exit(1)
}
