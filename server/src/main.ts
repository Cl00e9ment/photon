import express from 'express'
import path from 'path'
import {isLogged, loggedOnly, logIn, logOut, sessionMiddleware} from './session'
import {index, multipartMiddleware, processUploadedFile, sendMedia} from './media'
import {devMode} from './config'
import {parseDate} from './utils'
import {exiftool} from 'exiftool-vendored'
import compression from 'compression'
// @ts-ignore
import faviconPath from 'url:../res/favicon.ico'

const wwwDir = path.resolve(__dirname, '../client')
const port = devMode ? 8080 : 80

const publicSite = express.static(path.resolve(wwwDir, 'public'))
const privateSite = express.static(path.resolve(wwwDir, 'private'))
const app = express()

app.set('trust proxy', 1) // trust first proxy
app.use(sessionMiddleware)

app.use(compression())

// auto-login with query parameter
app.get('/', async (req, res, next) => {
	if ('key' in req.query) {
		// try to log-in with query parameter if present
		await logIn(req)
		// remove key from URL
		res.redirect('/')
	} else {
		next()
	}
})

app.get('/favicon.ico', (req, res) => {
	res.sendFile(faviconPath.substring(7)) // remove file:// prefix
})

app.post('/login', express.json(), async (req, res) => {
	res.sendStatus(await logIn(req) ? 200 : 403)
})

app.post('/logout', (req, res) => {
	logOut(req)
	res.redirect('/')
})

app.post('/upload', loggedOnly, multipartMiddleware.single('file'), async (req, res, next) => {
	const uploadDate = parseDate(req.body.uploadDate)
	if (uploadDate && uploadDate > Date.now()) {
		res.sendStatus(400)
		return
	}
	const media = await processUploadedFile(req.file, uploadDate)
	if (media) {
		res.status(200).send(media)
	} else {
		res.sendStatus(400)
	}
})

app.get('/index.json', loggedOnly, (_, res) => {
	res.send(index)
})

app.get('/media/:id', loggedOnly, sendMedia)

app.use(async (req, res, next) => {
	(await isLogged(req) ? privateSite : publicSite)(req, res, next)
})

app.all('*', (_, res) => {
	res.sendStatus(404)
})

const server = app.listen(port, () => {
	console.log(`server is listening on port ${port}`)
})

process.on('SIGINT', async () => {
	console.log('gracefully stopping')
	server.close()
	await exiftool.end(true)
	process.exit()
})
