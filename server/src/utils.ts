import childProcess from 'child_process'
import {DateTime} from 'luxon'

export function exec(command: string): Promise<{ stdout: string, stderr: string }> {
	return new Promise((resolve, reject) => {
		childProcess.exec(command, (error, stdout, stderr) => {
			if (error) {
				reject(error)
			} else {
				resolve({stdout, stderr})
			}
		})
	})
}

export function genUID(ids: string[]): string {
	const minLength = 6
	const maxLength = 32

	let length = minLength
	let id = genRandID(length)

	while (ids.includes(id)) {
		length = Math.min(maxLength, length + 1)
		id = genRandID(length)
	}

	return id
}

function genRandID(length: number): string {
	const unambiguousCharset = '23456789ABCEFGHKLMNPQRSTWXYZ'.split('')
	const charsetLength = unambiguousCharset.length

	let uid = ''
	for (let i = 0; i < length; ++i) {
		uid += unambiguousCharset[Math.floor(Math.random() * charsetLength)]
	}
	return uid
}

export function parseDate(raw: any): number | null {
	if (typeof raw !== 'string') {
		return null
	}
	const parsed = DateTime.fromISO(raw, {zone: 'UTC'})
	return parsed.isValid ? parsed.toMillis() : null
}
