import {NextFunction, Request, Response} from 'express'
import session from 'express-session'
import MemoryStore from 'memorystore'
import {randomBytes} from 'crypto'
import {compare} from 'bcryptjs'
import basicAuth from 'basic-auth'
import {devMode, keyHash} from './config'

type Session = session.Session & Partial<session.SessionData> & { logged?: boolean }

const sessionTTL = 86400000 // 24h

export const sessionMiddleware = session({
	cookie: {
		maxAge: sessionTTL,
		secure: !devMode,
		sameSite: 'strict',
	},
	store: new (MemoryStore(session))({
		checkPeriod: sessionTTL,
	}),
	secret: randomBytes(64).toString('hex'),
	resave: false,
	saveUninitialized: false,
})

export const loggedOnly = async (req: Request, res: Response, next: NextFunction) => {
	if (await isLogged(req)) {
		next()
	} else {
		// Do not give information of an existing page (404 instead of 401).
		res.sendStatus(404)
	}
}

export async function isLogged(req: Request): Promise<boolean> {
	// logged user
	if ((req.session as Session).logged) {
		return true
	}
	// programmatic access
	const basicAuthPass = basicAuth(req)?.pass
	if (basicAuthPass && await compare(basicAuthPass, keyHash)) {
		return true
	}
	// unauthorized
	return false
}

export async function logIn(req: Request): Promise<boolean> {
	const session = req.session as Session
	const key = req.body?.key ?? req.query?.key
	if (typeof key !== 'string') {
		return false
	}
	if (!await compare(key, keyHash)) {
		return false
	}
	session.logged = true
	session.save()
	return true
}

export function logOut(req: Request) {
	const session = req.session as Session
	session.logged = false
	session.destroy(err => {
		if (err) {
			console.error(err)
		}
	})
}
