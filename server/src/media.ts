import multer from 'multer'
import sharp from 'sharp'
import path from 'path'
import {mkdirSync, readFileSync} from 'fs'
import {rm, writeFile} from 'fs/promises'
import {exec, genUID} from './utils'
import {dataDir} from './config'
import {Request, Response} from 'express'
import {ExifDateTime, exiftool} from 'exiftool-vendored'

type File = Express.Multer.File

enum FileType {
	IMAGE,
	VIDEO,
	UNSUPPORTED,
}

interface Media {
	id: string,
	name: string,
	mimeType: string,
	creationDate?: number,
	uploadDate: number,
}

const tmpDir       = path.resolve(dataDir, 'tmp')
const originalsDir = path.resolve(dataDir, 'originals')
const thumbnailDir = path.resolve(dataDir, 'thumbnails')
const postersDir   = path.resolve(dataDir, 'posters')
const indexFile    = path.resolve(dataDir, 'index.json')

mkdirSync(tmpDir      , {recursive: true})
mkdirSync(originalsDir, {recursive: true})
mkdirSync(thumbnailDir, {recursive: true})
mkdirSync(postersDir  , {recursive: true})

export const index: {[key: string]: Media} = (() => {
	try {
		return JSON.parse(readFileSync(indexFile, 'utf-8'))
	} catch (_) {
		return {}
	}
})()

export const multipartMiddleware = multer({
	storage: multer.diskStorage({
		destination: originalsDir,
		filename: (req, file, cb) => {
			cb(null, genUID(Object.keys(index)))
		}
	}),
})

export const sendMedia = (req: Request, res: Response) => {
	const id = req.params.id
	if (!id.match(/^[23456789ABCEFGHKLMNPQRSTWXYZ]{6,32}$/)) {
		res.sendStatus(400)
		return
	}
	if (!(id in index)) {
		res.sendStatus(404)
		return
	}

	let dir: string
	if (req.query.thumb !== undefined) {
		dir = thumbnailDir
	} else if (req.query.poster !== undefined && index[id].mimeType.startsWith('video/')) {
		dir = postersDir
	} else {
		dir = originalsDir
	}

	res.setHeader('Content-Type', index[id].mimeType).sendFile(path.resolve(dir, id))
}

export async function processUploadedFile(file: File, uploadDate: number | null): Promise<Media | null> {
	const fileType = getFileType(file)

	if (fileType === FileType.UNSUPPORTED) {
		await rm(file.path)
		return null
	}

	let creationDate: number | undefined

	try {
		await createAssets(file, fileType)
		creationDate = await getCreationDate(file.path)
	} catch (err) {
		console.error(err)
		await rm(file.path)
		return null
	}

	index[file.filename] = {
		id: file.filename,
		name: file.originalname,
		mimeType: file.mimetype,
		creationDate: creationDate,
		uploadDate: uploadDate ?? Date.now(),
	}
	await writeFile(indexFile, JSON.stringify(index), 'utf-8')

	return index[file.filename]
}

async function createAssets(file: File, fileType: FileType) {
	const thumbnailPath = path.resolve(thumbnailDir, file.filename)

	switch (fileType) {
	case FileType.IMAGE:
		await createThumbnailFromImage(file.path, thumbnailPath)
		break

	case FileType.VIDEO:
		const framePath = await extractFrameFromVideo(file.path)
		await createThumbnailFromImage(framePath, thumbnailPath)
		await createPosterFromFrame(framePath, path.resolve(postersDir, file.filename))
		await rm(framePath)
		break
	}
}

async function createThumbnailFromImage(src: string, dest: string): Promise<void> {
	await sharp(src, {failOnError: false})  // try its best to handle corrupted images
		.resize(512, 512)
		.rotate()                                  // rotate according to EXIF metadata
		.jpeg({quality: 50})
		.toFile(dest)
}

async function createPosterFromFrame(src: string, dest: string): Promise<void> {
	await sharp(src)
		.jpeg({quality: 50})
		.toFile(dest)
}

async function extractFrameFromVideo(src: string): Promise<string> {
	src = path.resolve(src) // sanitize path before using it in an exec (shouldn't be necessary, but better safe than sorry)
	const framePath = path.resolve(tmpDir, path.basename(src) + '-frame.jpg')
	await exec(`ffmpeg -ss 00:00:00 -i ${src} -vframes 1 ${framePath}`)
	return framePath
}

async function getCreationDate(src: string): Promise<number | undefined> {
	const tags = await exiftool.read(src)

	let date: ExifDateTime
	if (typeof tags.DateTimeOriginal === 'object') {
		date = tags.DateTimeOriginal // date at which media was taken
	} else if (typeof tags.CreateDate === 'object') {
		date = tags.CreateDate // date at which media was saved to device (likely the same as DateTimeOriginal)
	} else {
		return undefined
	}

	return date.hasZone
		? date.toDateTime().toMillis()
		: date.toDateTime().setZone('Europe/Paris', {keepLocalTime: true}).toMillis() // TODO default to client zone
}

function getFileType(file: File): FileType {
	if (file.mimetype.startsWith('image/')) {
		return FileType.IMAGE
	}
	if (file.mimetype.startsWith('video/')) {
		return FileType.VIDEO
	}
	return FileType.UNSUPPORTED
}
