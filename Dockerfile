FROM cl00e9ment/node.js-builder:light AS builder
WORKDIR /dist/
COPY ./dist/ ./
RUN pnpm i --prod
RUN rm ./package.json ./pnpm-lock.yaml

FROM node:16-alpine
RUN apk add ffmpeg exiftool
WORKDIR /app/
COPY --from=builder /dist/ ./
ENV DATA_DIR /data/
ENV NODE_ENV production
EXPOSE 80
CMD node --enable-source-maps ./server/main.js
